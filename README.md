# recipe app API proxy

NGINX proxy app for recipe app

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of app to forward to requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)
